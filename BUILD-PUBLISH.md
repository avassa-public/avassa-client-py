# Build and publish

Instructions from https://realpython.com/pypi-publish-python-package/.

## Set version
Set updated version in pyproject.toml

## Build
```shell
pyproject-build
```
Creates a directory called `dist`.

## Upload
### Test Upload
If you want to upload to test pypi: 
**NOTE** this is a separate login and is not required for a release.
```shell
twine upload -r testpypi dist/*
```

### Upload
```shell
twine upload dist/*
```
